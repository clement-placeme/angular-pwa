// A simple api serving the content of .data/data.json on :4201/api/itinerary

const express = require('express');
const app = express();
const fs = require('fs');

app.get('/api/itinerary', (req, res) => {
    fs.readFile('./data/data.json', (err, json) => {
        let obj = JSON.parse(json);
        res.json(obj);
    });
});

app.listen(4201, () => console.log(`BOB Mock API listening on port 4201!`))