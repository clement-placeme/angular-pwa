import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  departure: string;
  destination: string;

  private routeParamsSubscribtion:  Subscription;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.routeParamsSubscribtion = this.route.queryParamMap.subscribe((queryParams: ParamMap) => {
      this.departure = queryParams.get('departure');
      this.destination =queryParams.get('destination');
    })
  }

  ngOnDestroy() {
    this.routeParamsSubscribtion.unsubscribe();
  }

}
