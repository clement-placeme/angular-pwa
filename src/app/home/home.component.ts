import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isHome: boolean;
  form: FormGroup;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      departure: new FormControl('Capitole'),
      destination: new FormControl('Gare Matabiau SNCF')
    });
  }

  onSubmit(): void {
    this.router.navigate(['/search'], {queryParams: this.form.value});
  }

}
